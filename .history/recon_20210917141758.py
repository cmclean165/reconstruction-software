import astra
import os
import cv2
import numpy as np
import numexpr as ne
import os
import shutil

import matplotlib.pyplot as plt

class reconstructor:

    def __init__(self, distance_source_origin, distance_origin_detector,
                       detector_pixel_size, detector_rows, detector_cols,
                                                    num_of_projections):

        self.distance_source_origin = distance_source_origin  # [mm]
        self.distance_origin_detector = distance_origin_detector  # [mm]
        self.detector_pixel_size = detector_pixel_size # [mm]
        self.detector_rows = detector_rows  # Vertical size of detector [pixels]
        self.detector_cols = detector_cols  # Horizontal size of detector [pixels]
        self.num_of_projections = num_of_projections
        self.angles = np.linspace(0, 2*np.pi, num=self.num_of_projections, endpoint=False)
        self.reconstruction = None

    def normalize(self, arr, flat, dark, cutoff=None, out=None):
        """ Performs a flat field image correction
            Inputs:
                - arr: input array
                - flat: flat field image
                - dark: dark field image
                - cutoff: max voue 
                - out: variable to output to (only for np.evaluate)
        """ 
        
        arr = np.array(arr, dtype=np.float32, copy=False)
        l = np.float32(1e-6)
        flat = np.mean(flat, axis=0, dtype=np.float32)
        dark = np.mean(dark, axis=0, dtype=np.float32)

        denom = ne.evaluate('flat-dark')
        ne.evaluate('where(denom<l,l,denom)', out=denom)
        out = ne.evaluate('arr-dark', out=out)
        ne.evaluate('out/denom', out=out, truediv=True)
        if cutoff is not None:
            cutoff = np.float32(cutoff)
            ne.evaluate('where(out>cutoff,cutoff,out)', out=out)

        return out

    def _remove_inf(self, arr):
        """ Removes numpy inf values from an array. replaces with largest float value 
            Inputs:
                - arr: matrix/array of values
        """

        # return if there are no infinite values
        if (np.max(arr) != np.inf):
            return arr

        # flatten the array and get its size
        flat = np.array(arr).flatten()
        arr_size = len(flat)

        # There should be
        # atleast two elements
        if (arr_size < 2):  
            print(" Invalid Input ")
            return arr

        flat[flat == np.inf] = np.min(flat)

        # Sort the array
        flat = np.sort(flat, axis=None)

        # set all infite values to the largest value in sorted
        arr[arr == np.inf] = flat[-1]

        return arr

    def minus_log(self, img):
        """ takes minus log of a matrix. this linearizes the image 
            Inputs: 
                - img: MxN image pixle matrix
        """

        img = img + 1
        img = ne.evaluate('-log(img)', out=img)

        return img

    def compute(self, projections, alg='FDK_CUDA', iter=100):
        """ Run the reconstruction algorithm 
            Inputs:
                - projections: series of xray projections:
                - alg: reconstruction aggorithm (from astra-toolbox)
                -iter: number of iterations for algebraic algs
        """

        proj_geom = \
        astra.create_proj_geom('cone', 1, 1, self.detector_rows, self.detector_cols, self.angles,
                                (self.distance_source_origin + self.distance_origin_detector) /
                                self.detector_pixel_size, 0)

        projections_id = astra.data3d.create('-sino', proj_geom, projections)

        vol_geom = astra.creators.create_vol_geom(self.detector_cols, self.detector_cols,
                                          self.detector_rows)
                                          
        reconstruction_id = astra.data3d.create('-vol', vol_geom, data=0)
        alg_cfg = astra.astra_dict(alg)
        alg_cfg['ProjectionDataId'] = projections_id
        alg_cfg['ReconstructionDataId'] = reconstruction_id
        # alg_cfg['MinConstraint'] = 0
        algorithm_id = astra.algorithm.create(alg_cfg)
        astra.algorithm.run(algorithm_id, iter) # perform 100 iterations
        self.reconstruction = astra.data3d.get(reconstruction_id)

        self.reconstruction[self.reconstruction < 0] = 0

        return self.reconstruction

    def _add_circular_mask(self):
        """ Adds a circular/oval mask to an image. R = 1/2 image height/width"""
        # read image
        hh, ww = self.reconstruction[0].shape[:2]
        hh2 = hh // 2
        ww2 = ww // 2

        # define circles
        radius = hh2
        yc = hh2
        xc = ww2

        # draw filled circle in white on black background as mask
        mask = np.zeros_like(self.reconstruction[0])
        mask = cv2.circle(mask, (xc,yc), radius, (255,255,255), -1)

        # reconstruction = (reconstruction - np.min(reconstruction)) / (np.max(reconstruction) - np.min(reconstruction))


        for loc, img in enumerate(self.reconstruction):
            # apply mask to image
            self.reconstruction[loc] = cv2.bitwise_and(img, mask) 

        return self.reconstruction

    def save_npy(self, f_name='recon_data.npy'):
        """ Save reconstruction data in numpy format """
        np.save(f_name, self.reconstruction)

    def save_png(self, dir_path='astra_out'):
        """ Save all reconstruction images to the 'dir_path' folder """
        if os.path.isdir(dir_path):
            shutil.rmtree(dir_path)

        os.mkdir(dir_path)

        for loc, img in enumerate(self.reconstruction):
            name = dir_path + '/' + str(loc) + '.png'
            # apply mask to image
            plt.imsave(name, img, cmap='gray')

        



if __name__ == "__main__":

    dark = cv2.imread('bracket/dark.png', cv2.IMREAD_GRAYSCALE)
    dark = cv2.rotate(dark, cv2.ROTATE_90_CLOCKWISE)
    flat = cv2.imread('bracket/flat.png', cv2.IMREAD_GRAYSCALE)
    flat = cv2.rotate(flat, cv2.ROTATE_90_CLOCKWISE)

    recon = reconstructor(1700, 300, 1, dark.shape[0], dark.shape[1], 180)
    
    projections = np.zeros((recon.detector_rows, recon.num_of_projections, recon.detector_cols))
    print(dark.shape)

    count = 0
    for i in range(0, 360, 2):
        name = str(i)
        while len(name) < 4:
            name = '0' + name

        name = name + '.png'
        filename = os.path.join('bracket/', name)
        img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
        img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
        
        #flat field correction
        img = recon.normalize(img, flat, dark)
        img = recon.minus_log(img)

        projections[:, count, :] = img
        count += 1

    # compute the reconstruction
    recon.compute(projections)

    plt.imsave('sinogram.png', projections[130], cmap='gray')
    plt.imsave('test.png', recon.reconstruction[130,:,:], cmap='gray')
    plt.imsave('test2.png', recon.reconstruction[:,130,:], cmap='gray')

    r = recon.reconstruction.copy()

    out1 = cv2.VideoWriter('top.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 24, (r.shape[0],r.shape[1]))

    count = 0
    for i in range(len(r)):
        out1.write(img)
        count += 1
        if count == 250: 
            break

    out1.release()
    
    # recon.save_npy()
    # recon.save_png()
    
